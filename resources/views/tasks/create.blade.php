@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h2>Create Task</h2>
                <form action="{{ route('tasks.store') }}" method="POST">
                    @csrf
                    <div class="card">
                        <div class="card-body">
                            <input type="text" class="form-group" name="name" placeholder="name...">
                        </div>
                        @error('name')
                        <span id="name-error" class="error text-danger" style="display: block;">
                                        {{ $message }}</span>
                        @enderror
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <input type="text" class="form-group" name="content" placeholder="content...">
                        </div>
                    </div>
                    <button class="btn btn-success">submit</button>
                </form>
            </div>
        </div>
    </div>

@endsection
