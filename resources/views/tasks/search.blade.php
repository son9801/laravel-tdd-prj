@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="d-flex justify-content-between align-items-center">
                    <h4 class="mb-0">TASK LIST</h4>
                    <div class="d-flex justify-content-center align-items-center flex-grow-1">
                        <form class="d-flex" method="GET" action="{{ route('tasks.search') }}">
                            <div class="input-group">
                                <input class="form-control" type="search" placeholder="Search" aria-label="Search"
                                       name="searchKey" value="{{ request('search') }}">
                                <button class="btn btn-outline-success" type="submit">Search</button>
                            </div>
                        </form>
                    </div>
                    <div class="d-flex justify-content-end align-items-center">
                        <a href="{{ route('tasks.create') }}" class="btn btn-primary">Add</a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row justify-content-center">
                    <table class="table table-strips">
                        <tr>
                            <th style="width: 20%;">ID</th>
                            <th style="width: 30%;">Name</th>
                            <th style="width: 50%;">Content</th>
                            <th>Action</th>
                        </tr>
                        @foreach($searchResult as $task)
                            <tr>
                                <th> {{$task->id}}</th>
                                <th> {{$task->name}}</th>
                                <th> {{$task->content}}</th>
                                <th>
                                    <div class="btn-group">
                                        <a href="{{ route('tasks.show', $task->id) }}"
                                           class="btn btn-warning">Update </a> <br/>
                                        <span class="mx-1"></span>
                                        <a href="{{ route('tasks.delete', $task->id) }}"
                                           class="btn btn-danger">Delete</a>
                                    </div>
                                </th>
                            </tr>
                        @endforeach
                    </table>
                    {{--                    {{$search->links()}}--}}
                </div>
            </div>
        </div>

    </div>
@endsection
