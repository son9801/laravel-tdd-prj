@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h2>Create Task</h2>
                <form action="{{ route('tasks.update', $task->id) }}" method="POST">
                    @csrf
                    @method('PUT')

                    <div class="card">
                        <div class="card-body">
                            <input type="text" class="form-group" value="{{ $task->name }}" name="name">
                        </div>
                        @error('name')
                        <span id="name-error" class="error text-danger" style="display: block;">
                                        {{$message }}</span>
                        @enderror

                        <div class="card">
                            <div class="card-body">
                                <input type="text" class="input-group mb-3"  value="{{ $task->content}}" name="content">
                            </div>
                        </div>
                        <button class="btn btn-success">submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection
