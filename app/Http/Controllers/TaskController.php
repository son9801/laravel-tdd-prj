<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTaskRequest;
use App\Http\Requests\UpdateTaskRequest;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TaskController extends Controller
{

    protected $task;

    public function __construct(Task $task)
    {
        $this->task = $task;
    }

    public function index()
    {
        $tasks = $this->task->latest('id')->paginate(10);

        return view('tasks.index', compact('tasks'));
    }

    public function store(CreateTaskRequest $request)
    {
        $this->task->create($request->all());

        return redirect()->route('tasks.index');
    }

    public function create()
    {
        return view('tasks.create');
    }

    public function show(string $id)
    {
        $task = $this->task->findorfail($id);

        return view('tasks.update', compact('task'));
    }

    public function update(UpdateTaskRequest $request, string $id)
    {
        $task = $this->task->findorfail($id);
        $task->update($request->all());

        return redirect()->route('tasks.index');
    }

    public function delete(Request $request, string $id)
    {
        $task = $this->task->findorfail($id);
        $task->delete();

        return redirect()->route('tasks.index');
    }

    public function search(Request $request)
    {
        $searchKey = $request->searchKey;

        if ($searchKey !== null) {
            $searchResult = $this->task->where('name', 'LIKE', '%' . $searchKey . '%')->get();

            return view('tasks.search', compact('searchResult'));
        } else {
            return redirect()->route('tasks.index')->with('message', 'Vui lòng nhập từ khoá để tìm kiếm');
        }
    }

}
