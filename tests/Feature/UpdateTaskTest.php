<?php

namespace Tests\Feature;


use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UpdateTaskTest extends TestCase
{


    public function getListTaskRoute()
    {
        return route('tasks.index');
    }

    public function getUpdateTaskViewRoute($id)
    {
        return route('tasks.show', ['id' => $id]);
    }

    public function putUpdateTaskRoute($id)
    {
        return route('tasks.update', ['id' => $id]);
    }
    /**
     * @test
     */
    public function authenticated_user_can_see_update_task_form()
    {
        $this->actingAs(User::factory()->make());
        $task = Task::factory()->create();
        $response = $this->get($this->getUpdateTaskViewRoute($task->id));

        $response->assertViewIs('tasks.update');
        $response->assertSee('name');
        $response->assertSee('content');
    }

    /**
     * @test
     */
    public function unauthenticated_user_cant_see_update_task_form()
    {
        $task = Task::factory()->create();
        $response = $this->get($this->getUpdateTaskViewRoute($task->id));

        $response->assertRedirect('/login');
    }

    /**
     * @test
     */
    public function authenticated_user_can_update_task()
    {
        $this->actingAs(User::factory()->make());
        $task = Task::factory()->create();
        $response = $this->put($this->putUpdateTaskRoute($task->id), $task->toArray());

        $response->assertStatus(302);
        $this->assertDatabaseHas('tasks', $task->toArray());
        $response->assertRedirect($this->getListTaskRoute());
    }

    /**
     * @test
     */
    public function authenticated_user_cant_update_task_if_name_field_null()
    {
        $this->actingAs(User::factory()->make());
        $task = Task::factory()->create();
        $data = [
            'name' => null,
            'content' => $task->content
        ];
        $response = $this->put($this->putUpdateTaskRoute($task->id), $data);

        $response->assertSessionHasErrors(['name']);
    }

    /**
     * @test
     */
    public function authenticated_user_cant_update_task_if_validate_error()
    {
        $this->actingAs(User::factory()->make());
        $task = Task::factory()->create();
        $data = [
            'name' => null,
            'content' => $task->content
        ];
        $response = $this->from($this->getUpdateTaskViewRoute($task->id))->put($this->putUpdateTaskRoute($task->id), $data);

        $response->assertRedirect($this->getUpdateTaskViewRoute($task->id));
    }

}

