<?php

namespace Tests\Feature;

use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class SearchTaskTest extends TestCase
{
    public function getSearchResult($searchKey)
    {
        return route('tasks.search', ['searchKey' => $searchKey]);
    }

    /**
     * @test
     */
    public function user_can_get_search_result_with_keyword()
    {
        $task = Task::factory()->create();
        $response = $this->get($this->getSearchResult($task->name));

        $response->assertStatus(200);
        $response->assertViewIs('tasks.search');
        $response->assertSeeText($task->name);
    }


    /**
     * @test
     */
    public function user_cant_get_search_result_with_null_keyword()
    {
        $response = $this->get($this->getSearchResult(''));

        $response->assertRedirect(route('tasks.index'));
        $response->assertSessionHas('message', 'Vui lòng nhập từ khoá để tìm kiếm');
    }
}
