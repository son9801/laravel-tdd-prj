<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DeleteTaskTest extends TestCase
{
    public function deleteTaskRoute($id)
    {
        return route('tasks.delete', ['id' => $id]);
    }

    /**
     * @test
     */
    public function authenticated_user_can_delete_task()
    {
        $this->actingAs(User::factory()->make());
        $task = Task::factory()->create();
        $response = $this->get($this->deleteTaskRoute($task->id));

        $response->assertStatus(302);
    }

    /**
     * @test
     */
    public function authenticated_user_cant_delete_task_if_task_not_exist()
    {
        $this->actingAs(User::factory()->make());
        $taskId = -1;
        $response = $this->get($this->deleteTaskRoute($taskId));

        $response->assertStatus(404);
    }
}
