<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateNewTaskTest extends TestCase
{

    public function getCreateRoute()
    {
        return route('tasks.store');
    }

    public function getCreateTaskViewRoute()
    {
        return route('tasks.create');
    }

    /**
     * @test
     */
    public function authenticated_user_can_create_new_task()
    {

        $this->actingAs(User::factory()->make());
        $task = Task::factory()->make()->toArray();
        $response = $this->post($this->getCreateRoute(), $task);

        $response->assertStatus(302);
        $this->assertDatabaseHas('tasks', $task);
        $response->assertRedirect(route('tasks.index'));
    }

    /**
     * @test
     */
    public
    function unauthenticated_user_cant_create_new_task()
    {
        $task = Task::factory()->make()->toArray();
        $response = $this->post($this->getCreateRoute(), $task);

        $response->assertRedirect('/login');

    }

    /**
     * @test
     */
    public function authenticated_user_cant_create_new_task_if_name_field_null()
    {
        $this->actingAs(User::factory()->make());
        $task = Task::factory()->make(['name' => null])->toArray();
        $response = $this->post($this->getCreateRoute(), $task);

        $response->assertSessionHasErrors(['name']);
    }

    /**
     * @test
     */
    public function authenticated_user_can_view_create_task_form()
    {
        $this->actingAs(User::factory()->make());
        $response = $this->get($this->getCreateTaskViewRoute());

        $response->assertViewIs('tasks.create');
    }

    /**
     * @test
     */
    public function unauthenticated_user_cant_see_create_task_form()
    {
        $response = $this->get($this->getCreateTaskViewRoute());

        $response->assertRedirect('/login');
    }

    /**
     * @test
     */
    public function authenticated_user_can_name_required_if_validate_error()
    {
        $this->actingAs(User::factory()->make());
        $task = Task::factory()->make(['name' => null])->toArray();

        $response = $this->from($this->getCreateTaskViewRoute())->post($this->getCreateRoute(), $task);
        $response->assertRedirect($this->getCreateTaskViewRoute());
    }
}
